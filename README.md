# PCB Versioning


This document details how version numbers are assigned to PCB designs.
It is written in the spirit of the
[semantic versioning specification](http://semver.org) for software, but adapted for
hardware purposes.

Each release of a PCB design is allocated a version number of the form
MAJOR.MINOR.PATCH.  Given this number, increment the:

 1. MAJOR version when modifications are made that will introduce
    incompatibilities with external dependencies (e.g. firmware,
    mechanical housings, and anything else that depends on the
    design).
 2. MINOR version when functionality is added in a
    backwards-compatible manner.
 3. PATCH when backwards-compatible bug fixes are made to the design.


## Examples

Here are some examples of when each version number needs to be
increased.

MAJOR is incremented when:

 * The board outline changes.
 * Mounting holes are moved.
 * Components are moved on the board, since the physical envelope of
   the board is changed
 * The pin an LED is connected to on a microcontroller is changed.
 * The pinout of a connector is changed.

MINOR is incremented when:

 * Polarity markings are added to a connector.

PATCH is incremented when:

* The pinout of a component footprint is corrected.
* The supplier of a component is changed in the BOM.
* Scripts that generate the board gerbers are changed.

## License

Copyright 2018 by Adventurous Machines Ltd.  This work is licensed under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).  Please attribute Adventurous Machines with a link to [adventurousmachines.com](https://adventurousmachines.com).
